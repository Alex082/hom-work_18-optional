// Создаём функцыю клонирования

function cloneObject(object) {
  // Делаем проверку на является ли обьект обьектом

  if (typeof object !== "object" || object === null) {
    return object;
  }

  // Создаём переменную для нового обьекта и вней пишем проверку

  const clones = Array.isArray(object) ? [] : {};

  // делаем цикль для перехода по всем ключам в обьекте

  for (let key in object) {
    if (object.hasOwnProperty(key)) {
      clones[key] = cloneObject(object[key]);
    }
  }

  // Делаем возврат clones

  return clones;
}
